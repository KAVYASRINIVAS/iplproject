//To calculate number of matches per year
function matchesPerYear(matches) {
    const matchPerYear = {};

    for (let key in matches) {
        if (!matchPerYear[matches[key].season]) {
            matchPerYear[matches[key].season] = 1;
        } else {
            matchPerYear[matches[key].season]++;
        }
    }
    return matchPerYear;
}


//To calculate number of matches wom per team per year
function matchesWonPerTeam(matches) {
    const matchesWon = {};

    for (let key in matches) {
        let year = matches[key].season;
        if (!matchesWon[year]) {
            matchesWon[year] = {}
        }
    }

    for (let key in matches) {
        let year = matches[key].season;
        let team = matches[key].winner;

        if (team in matchesWon[year]) {
            matchesWon[year][team] += 1;
        } else {
            matchesWon[year][team] = 1;
        }
    }
    return matchesWon;

}

//To calculate number of extra runs conceded per team in the year 2016
function extraRunsPerTeam(matchesData, deliveriesData, year = 2016) {
    let matches = [];
    let extraRuns = {};

    matchesData.filter((match) => {
        if ((Number(match.season) === year)) {
            matches.push(Number(match.id));
        }
    });

    let start = matches[0];
    let end = matches[matches.length - 1];

    let delivery = deliveriesData.filter((deliveries) => {
        if (deliveries.match_id >= start && deliveries.match_id <= end) {
            return true;
        } else {
            return false;
        }
    })

    delivery.forEach(deliveries => {
        let team = deliveries.bowling_team;
        let extra = deliveries.extra_runs;

        if (extraRuns.hasOwnProperty(team)) {
            extraRuns[team] += Number(extra);
        } else {
            extraRuns[team] = Number(extra);
        }

    });
    return extraRuns;

}
//To calculate top 10 economical bowlers in the year 2015
function topEconomicalBowlers(matchData, deliveriesData, year = 2015) {
    let topBowlers = {};
    let matchYear = [];

    matchData.filter((match) => {
        if (Number(match.season) === year) {
            matchYear.push(Number(match.id));
        }
    });

    //console.log(matchYear);

    const start = matchYear[0];
    const end = matchYear[matchYear.length - 1];

    let bowlers = deliveriesData.filter((delivery) => {
        if (delivery.match_id >= start && delivery.match_id <= end) {
            return true;
        } else {
            return false;
        }
    });

    bowlers.forEach((delivery) => {
        let name = delivery.bowler;
        let total = delivery.total_runs;

        if (topBowlers.hasOwnProperty(name)) {
            topBowlers[name].runs += Number(total);
            topBowlers[name].balls += 1;
        }
        else {

            topBowlers[name] = {
                runs: Number(total),
                balls: 1,
                economy: 0
            }
        }

        if (delivery.extra_runs === 0) {
            topBowlers[name].balls += 1;
        }
        let runs = topBowlers[name].runs;
        let balls = topBowlers[name].balls;
        let overs = balls / 6;
        let economy = (runs / overs);
        topBowlers[name].economy = Number(economy.toFixed(2));
    });

    let economicBowlers = [];

    for (let key in topBowlers) {
        economicBowlers[key] = topBowlers[key].economy;
    }

    let result = Object.entries(economicBowlers).sort((a, b) => a[1] - b[1]).slice(0, 10);
    let finalResult = {};

    for (let i = 0; i < result.length; i++) {
        let key = result[i][0];
        let value = result[i][1];
        finalResult[key] = value;

    }

    return finalResult;
}


// To find the number of times each team won the toss and also won the match
function winnerTossandMatch(matchData) {
    let countWinner = {};
    const team = [...new Set(matchData.map(match => match.team1))];

    let result = {};
    matchData.forEach((match) => {
        let matchWinner = match.winner;
        let tossWinner = match.toss_winner;

        if (matchWinner === tossWinner) {
            if (result.hasOwnProperty(matchWinner)) {
                result[matchWinner] += 1
            }
            else {
                result[matchWinner] = Number(1);
            }
        }
    });
    return result;
}


//To find a player who has won the highest number of Player of the Match awards for each season
function playerOfTheMatch(matchData) {
    let topPlayer = matchData.reduce((playerCount, match) => {
        let year = match.season;
        let player = match.player_of_match;

        if (!playerCount[year]) {
            playerCount[year] = {};
        }

        if (!playerCount[year][player]) {
            playerCount[year][player] = 1;
        } else {
            playerCount[year][player] += 1;
        }
        return playerCount
    }, {});

    let playerArray = Object.entries(topPlayer).reduce((result, players) => {
        let year = players[0];
        let player = players[1];
        let playerMatchCount = Object.entries(player).reduce((sortPlayer, playerToSort) => {
            let playerName = playerToSort[0];
            let playerCount = playerToSort[1];
            if (playerCount === sortPlayer.count) {
                sortPlayer.player.push(playerName);
            } else if (playerCount > sortPlayer.count) {
                sortPlayer.player = [playerName];
                sortPlayer.count = playerCount;
            }
            return sortPlayer;
        }, { 'player': [], 'count': 0 });
        result[year] = playerMatchCount;
        return result;
    }, {});
    return playerArray;
}

// to calculate the strike rate of a batsman for each season
function strikeRate(matchData, deliveriesData) {

    let topBatsmen = {};
    let matchYear = [];
    let final = {};
    let finalStrike={};

    let years = [...new Set(matchData.map(match => Number(match.season)))].sort((a,b) => a-b);


    for (let index = 0; index < years.length; index++) {

        matchData.filter((match) => {
            if (Number(match.season) === years[index]) {
                matchYear.push(Number(match.id));
            }
        });

        let start = matchYear[0];
        let end = matchYear[matchYear.length - 1];

        let strike = deliveriesData.filter((delivery) => {
            if (delivery.match_id >= start && delivery.match_id <= end) {
                return true;
            } else {
                return false;
            }
        });

        strike.forEach((delivery) => {
            let name = delivery.batsman;
            let totalRuns = delivery.batsman_runs;

            if (topBatsmen.hasOwnProperty(name)) {
                topBatsmen[name].runs += Number(totalRuns);
                topBatsmen[name].balls += 1;
            }
            else {
                topBatsmen[name] = {
                    runs: Number(totalRuns),
                    balls: 1,
                    strikerate: Number(1)
                }
            }
            if (delivery.extra_runs === 0) {
                topBowlers[name].balls += 1;
            }
            // let runs = topBatsmen[name].runs;
            // let balls = topBatsmen[name].balls;
            // let rate = (runs / balls);
            // topBatsmen[name].strikerate = Number(rate.toFixed(2));
        });
        for(let player in topBatsmen){
            let runs = topBatsmen[player].runs;
            let balls = topBatsmen[player].balls;
            let rate = (runs / balls)*100;
            finalStrike[player] = Number(rate.toFixed(2));
        }
        final[years[index]] = finalStrike;
    }
    return final;
}
// To find the highest number of times one player has been dismissed by another player
function maxPlayerDismissed(deliveriesData) {
    let playerDismissed = [];
    for (let i = 0; i < deliveriesData.length; i++) {
        if (deliveriesData[i]['player_dismissed'] !== "") {
            playerDismissed.push([deliveriesData[i]['batsman'], deliveriesData[i]['bowler']]);
        }
    }
    let playerCount = [];

    for (let players of playerDismissed) {
        let batsman = players[0];
        let bowler = players[1];
        let count = 0;
        for (let pair of playerDismissed) {
            if (pair[0] == batsman && pair[1] == bowler) {
                count = count + 1;
            }
        }
        playerCount.push(count);
    }
    let maxDismissel = playerCount.reduce((a, b) => Math.max(a, b));

    let indexArray = [];
    for (let i = 0; i < playerCount.length; i++) {
        if (playerCount[i] == maxDismissel) {
            indexArray.push(i);
        }
    }

    let result = {}

    for (let index of indexArray) {

        batsman = playerDismissed[index][0]
        bowler = playerDismissed[index][1]

        let key = (batsman + "," + bowler)

        if (result.hasOwnProperty(key)) {
            continue
        }
        else {
            result[key] = maxDismissel
        }

    }
    return result;

}

//  To find the bowler with the best economy in super overs

function topEconomicalBowlersSuperOver(matchData, deliveriesData, year = 2017) {
    let topBowlers = {};
    let matchYear = [];

    matchData.filter((match) => {
        if (Number(match.season) === year) {
            matchYear.push(Number(match.id));
        }
    });

    const start = matchYear[0];
    const end = matchYear[matchYear.length - 1];

    let bowler = deliveriesData.filter((delivery) => {
        if (delivery.match_id >= start && delivery.match_id <= end) {
            return true;
        } else {
            return false;
        }
    });

    let bowlers = bowler.filter((delivery) => {
        if (delivery.is_super_over == 1) {
            return true;
        }
        else {
            return false;
        }
    })

    bowlers.forEach((delivery) => {
        let name = delivery.bowler;
        let total = delivery.total_runs;

        if (topBowlers.hasOwnProperty(name)) {
            topBowlers[name].runs += Number(total);
            topBowlers[name].balls += 1;
        }
        else {

            topBowlers[name] = {
                runs: Number(total),
                balls: 1,
                economy: 0
            }
        }

        if (delivery.extra_runs === 0) {
            topBowlers[name].balls += 1;
        }
        let runs = topBowlers[name].runs;
        let balls = topBowlers[name].balls;
        let overs = balls / 6;
        let economy = (runs / overs);
        topBowlers[name].economy = Number(economy.toFixed(2));
    });

    let economicBowlers = [];

    for (let key in topBowlers) {
        economicBowlers[key] = topBowlers[key].economy;
    }

    let result = Object.entries(economicBowlers).sort((a, b) => a[1] - b[1]).slice(0, 10);
    let finalResult = {};

    for (let i = 0; i < result.length; i++) {
        let key = result[i][0];
        let value = result[i][1];
        finalResult[key] = value;

    }

    return finalResult;
}

module.exports = {
    matchesPerYear,
    matchesWonPerTeam,
    extraRunsPerTeam,
    topEconomicalBowlers,
    winnerTossandMatch,
    playerOfTheMatch,
    strikeRate,
    maxPlayerDismissed,
    topEconomicalBowlersSuperOver
} 