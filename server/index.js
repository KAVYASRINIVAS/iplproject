
let csvToJson = require('convert-csv-to-json');
let path = require('path');
let fs=require('fs');

const ipl = require('./ipl.js');
const { readFile } = require('convert-csv-to-json/src/util/fileUtils');
const { writeFile } = require('convert-csv-to-json/src/util/fileUtils');



const matchData = csvToJson.fieldDelimiter(',').getJsonFromCsv('./data/matches.csv');
const deliveriesData = csvToJson.fieldDelimiter(',').getJsonFromCsv('./data/deliveries.csv');

const matchesPerYear=ipl.matchesPerYear(matchData,deliveriesData);
const matchesWonPerTeam=ipl.matchesWonPerTeam(matchData,deliveriesData);
const extraRunsPerTeam=ipl.extraRunsPerTeam(matchData,deliveriesData);
const topEconomicalBowlers=ipl.topEconomicalBowlers(matchData,deliveriesData);
const winnerTossandMatch=ipl.winnerTossandMatch(matchData);
const playerOfTheMatch=ipl.playerOfTheMatch(matchData);
const strikerate=ipl.strikeRate(matchData,deliveriesData);
const maxPlayerDismissed=ipl.maxPlayerDismissed(deliveriesData);
const topEconomicalBowlersSuperOver=ipl.topEconomicalBowlersSuperOver(matchData,deliveriesData);


writeFile(JSON.stringify(matchesPerYear, null, 2),
            path.resolve(__dirname, "../public/output/matchesPerYear.json")
        );

writeFile(JSON.stringify(matchesWonPerTeam, null, 2),
            path.resolve(__dirname, "../public/output/matchesWonPerTeam.json")
        );

writeFile(JSON.stringify(extraRunsPerTeam,null,2),
path.resolve(__dirname,'../public/output/extraRunsPerTeam.json')
);

writeFile(JSON.stringify(topEconomicalBowlers,null,2),
path.resolve(__dirname,'../public/output/topEconomicalBowlers.json')
);

writeFile(JSON.stringify(winnerTossandMatch,null,2),
path.resolve(__dirname,'../public/output/winnerTossandMatch.json')
);

writeFile(JSON.stringify(playerOfTheMatch,null,2),
path.resolve(__dirname,'../public/output/topPlayerOfTheMatch.json')
);

writeFile(JSON.stringify(strikerate,null,2),
path.resolve(__dirname,'../public/output/strikeRateOfPlayer.json')
);

writeFile(JSON.stringify(maxPlayerDismissed,null,2),
path.resolve(__dirname,'../public/output/maxPlayerDismissed.json')
);

writeFile(JSON.stringify(topEconomicalBowlersSuperOver,null,2),
path.resolve(__dirname,'../public/output/topEconomicalBowlersSuperOver.json')
);